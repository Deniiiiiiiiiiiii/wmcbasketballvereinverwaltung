import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {FormsModule} from "@angular/forms";
import { HomePageComponent } from './home-page/home-page.component';
import { BreakpointObserver } from '@angular/cdk/layout';
import { JoinNowComponent } from './join-now/join-now.component';
import {DatePipe} from "@angular/common";
import {Router} from "@angular/router";
@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    JoinNowComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    DatePipe

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
