import {Component, OnInit} from '@angular/core';
import {BreakpointObserver} from "@angular/cdk/layout";
import {Team} from "../Team";
import {Spieler} from "../Spieler";
import {Trainer} from "../Trainer";

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit{

  teams: Team[] = [];
  players: Spieler[] = [];
  trainers: Trainer[] = [];


  team!: Team;
  player!: Spieler;
  trainer!: Trainer;


  isChecked = false;
  media = true;
  slideIndex = 1;

  constructor(private breakpoint: BreakpointObserver) {
  }

  plusSlides(n: number){
    this.showSlides(this.slideIndex += n);
  }

  currentSlide(n: number){
    this.showSlides(this.slideIndex = n);
  }

  showSlides(n: number): void {
    let i: number;
    let slides = document.getElementsByClassName("mySlides");
    let dots = document.getElementsByClassName("dot");
    let slideIndex = n;
    if (slideIndex > slides.length) {slideIndex = 1}
    if (slideIndex < 1) {slideIndex = slides.length}
    for (i = 0; i < slides.length; i++) {
      slides[i].setAttribute("style", "display:none");
    }
    for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
    }
    slides[slideIndex-1].setAttribute("style", "display:block");
    dots[slideIndex-1].className += " active";
  }

  toggle(){
    this.isChecked = !this.isChecked;
  }

  ngOnInit(): void {

    this.breakpoint.observe(['(max-width: 768px)']).subscribe(result =>{
      if (result.matches){
        console.log("Screen less then 768px");
        this.media = false;
      }else {
        this.media = true;
      }
    })

  }
}
