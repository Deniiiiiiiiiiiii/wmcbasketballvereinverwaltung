import {Spieler} from "./Spieler";
import {Trainer} from "./Trainer";

export class Team{

  constructor(
    public id: number,
    public name: string,
    public standort: string,
    public herkunft: Date,
    public spieler: Spieler[],
    public trainer: number
  ) {

  }
}
