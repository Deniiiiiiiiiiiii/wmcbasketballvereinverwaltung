import {Component, OnInit} from '@angular/core';
import {NgForm} from "@angular/forms";
import {Spieler} from "../Spieler";
import {Team} from "../Team";
import {Route, Router} from "@angular/router";

@Component({
  selector: 'app-join-now',
  templateUrl: './join-now.component.html',
  styleUrls: ['./join-now.component.css']
})
export class JoinNowComponent implements OnInit{

  verein: Team = new Team(0, "", "", new Date(), [], 0);
  vereinList: Team[] = [];

  spieler: Spieler = new Spieler(0, "", "", new Date(), 0, "", 0);

  constructor(private router: Router) {
  }

  goBack(){
    this.router.navigate(['']);
  }

  onSubmit(form: NgForm){

  }

  ngOnInit(): void {

  }
}
