import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomePageComponent} from "./home-page/home-page.component";
import {JoinNowComponent} from "./join-now/join-now.component";

const routes: Routes = [{
  path: '', component: HomePageComponent
},
  {
    path: 'join', component:JoinNowComponent
  }];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
