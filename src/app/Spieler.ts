export class Spieler{

  constructor(

    public id: number,
    public vorname: string,
    public nachname: string,
    public geburtsdatum: Date,
    public spieler_nummer: number,
    public position: string,
    public verein_id: number
  ) {
  }
}
