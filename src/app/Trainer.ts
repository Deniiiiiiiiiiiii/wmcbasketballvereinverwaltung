export class Trainer{

  constructor(
    public id: number,
    public vorname: string,
    public nachname: string,
    public verein_id: number
  ) {
  }
}
